<?php 
session_start();
if(isset($_SESSION['login']) and !empty($_SESSION['login'])){
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trombinoscope de l'entreprise</title>
    <script src="https://kit.fontawesome.com/962a7b60f8.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
    <link rel="stylesheet" href="styleAnnuaire.css">
    <script src="listeDerProfil.js" defer></script>
</head>
<body>
<?php
    if($_SESSION['groupe'] === 'ADM' OR $_SESSION['groupe'] === 'REH'){
?>
<div class="menu">
            <div  class="dropdown" id="dropdownMenu">
                <a class="profile-icon" onclick="toggleDropdown()">
                    <div><i class="fa-solid fa-user"></i></div>
                </a>
            </div>   
            <a href="siteintranet.php">  
            <i class="fa-solid fa-house"></i>
            </a>
            <a href='creActualites.php'>
                <div class='iconeStylo' style="margin-bottom: 100%;"><i class='fa-solid fa-pen-fancy'></i></div>
            </a>  
            <div><i class="fa-brands fa-twitter"></i></div>
            <div><i class="fa-brands fa-instagram"></i></div>
            <div class="iconeLin" style="margin-bottom: 100%;"><i class="fa-brands fa-linkedin"></i></div>
            <div class="iconeI"><i class="fa-solid fa-circle-info"></i></div>
</div>
<?php 
    } else {
?>
    <div class="menu">
            <div  class="dropdown" id="dropdownMenu">
                <a class="profile-icon" onclick="toggleDropdown()">
                    <div><i class="fa-solid fa-user"></i></div>
                </a>
            </div>   
            <a href="siteintranet.php">  
            <div style="margin-bottom: 100%;"><i class="fa-solid fa-house"></i></div>
            </a>  
            <div><i class="fa-brands fa-twitter"></i></div>
            <div><i class="fa-brands fa-instagram"></i></div>
            <div class="iconeLin" style="margin-bottom: 200%;"><i class="fa-brands fa-linkedin"></i></div>
            <div class="iconeI"><i class="fa-solid fa-circle-info"></i></div>
</div>
<?php    
        }
?>
<div id="directory" class="directory">
    <fieldset>
<div class="legend">
    <div class="dot points blue"><h3>Directeur</h3></div>
    <div class="dot points green"><h3>Responsable</h3></div>
    <div class="dot points red"><h3>Employé</h3></div>
</div>
</fieldset>
    <?php
include("connexion.php");

// Récupérer les données des groupes
$query_groupes = "SELECT DISTINCT g.codeGrp, g.libelle FROM utilisateurs u INNER JOIN groupe g ON u.codeGrp = g.codeGrp";
$result_groupes = $connexion->query($query_groupes);

// Vérifier s'il y a des groupes
if ($result_groupes->rowCount() > 0) {
    // Parcourir les groupes
    while ($row_groupe = $result_groupes->fetch(PDO::FETCH_ASSOC)) {
        $groupe = $row_groupe['codeGrp'];
        $libelle = $row_groupe['libelle'];
        // Afficher le groupe 
        echo '<div class="group">'; // Groupe
        echo '<h2 style ="color: white";>' . $libelle . '</h2>';
        echo '<ul class="members id="members">';

        // Récupérer les employés associés à ce groupe
        $query_employes = "SELECT * FROM infosemployes i INNER JOIN utilisateurs u ON i.login = u.login INNER JOIN groupe g ON u.codeGrp = g.codeGrp WHERE u.codeGrp = :codeGrp";
        $stmt_employes = $connexion->prepare($query_employes);
        $stmt_employes->bindParam(':codeGrp', $groupe);
        $stmt_employes->execute();

        // Afficher les employés
        while ($row_employe = $stmt_employes->fetch(PDO::FETCH_ASSOC)) {
            echo '<div class="employe">';
            // Afficher les images 
            echo '<div class="image"><img src="' . $row_employe['chemin_image'] . '" alt="Image de lemployé" class="image-redimension"></div>';
            echo '<div class="info">';
                  
            // Générer une classe CSS en fonction de l'idposte
            $class = '';
            switch ($row_employe['leposte']) {
                case 'DIR':
                    $class = 'blue';
                    break;
                case 'RES':
                    $class = 'green';
                    break;
                case 'EMP':
                    $class = 'red';
                    break;
                default:
                    $class = 'black'; // Couleur par défaut si l'idposte n'est pas reconnu
            }

            // Afficher le rond coloré avec la classe CSS générée
            echo '<div class="dot ' . $class . ' point"></div>';
            echo '<div class="nom">' . $row_employe['prenom'] . ' ' . $row_employe['nom'] . '<p>' . $row_employe['tel'] . '</p>' . '<p>' . $row_employe['mail'] . '</p>' . '</div>';
            echo '</div>';
            echo '</div>';
        }

        // Fermer la liste des employés et le groupe
        echo '</ul>';
        echo '</div>';
    }
} else {
    echo "<li>Aucun groupe trouvé.</li>";
}
?>
</div>

</body>
</html>
<?php
}else{
    header("Location: connexionUser.html");
}
?>

