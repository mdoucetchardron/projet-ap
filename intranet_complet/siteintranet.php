<?php
// Récupération de la session
session_start();
include("connexion.php");

if(isset($_SESSION['login']) and !empty($_SESSION['login'])){

?>
<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="intranet.css" rel="stylesheet">
        <link rel="stylesheet" href="styleCalendar.css">
        <script src="https://kit.fontawesome.com/962a7b60f8.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
        <link rel="stylesheet" href="styleHorloge.css">
        <style>
            pie-chart {
                width: 12vw;
                height: 12vw;
            }
        </style>
        <script>
            // Rafraîchir les actualités toutes les 60 secondes
            setInterval(function() {
                window.location.reload();
            }, 3600000); // 60 secondes
        </script>
        <script src="carouseljs.js" async></script>
        <script src="app.js" defer></script>    
        <script src="listeDerProfil.js" defer></script>
        <script src="scriptCalendar.js" defer></script>
        <script src="scriptHorloge.js" defer></script>
        <script src="scriptToDoList.js" defer></script>
        <title>intranet</title>
    </head>
<body class="background">
    <div class="conteneur">
    <?php
    if($_SESSION['groupe'] === 'ADM' OR $_SESSION['groupe'] === 'REH'){
    ?>
        <div class="menu">
            <div  class="dropdown" id="dropdownMenu">
                <a class="profile-icon" onclick="toggleDropdown()">
                    <div><i class="fa-solid fa-user"></i></div>
                </a>
            </div> 
            <a href="annuaire.php">   
            <i class="fa-solid fa-address-book"></i>
            </a>
            
            <a href='creActualites.php'>
                <div class='iconeStylo' style="margin-bottom: 100%;"><i class='fa-solid fa-pen-fancy'></i></div>
            </a>           
            <a href="https://twitter.com/Accueil"> 
            <div class="iconeTwi"><i class="fa-brands fa-twitter"></i></div>
            </a>
            <a href="https://www.instagram.com/">
            <div class="iconeInsta"><i class="fa-brands fa-instagram"></i></div>
            </a>
            <a href="https://fr.linkedin.com/">
            <div class="iconeLin" style="margin-bottom: 200%;"><i class="fa-brands fa-linkedin"></i></div>
            </a>
            <div class="iconeI"><i class="fa-solid fa-circle-info"></i></div>
        </div>
        <?php 
            } else {
            ?>
            <div class="menu">
            <div  class="dropdown" id="dropdownMenu">
                <a class="profile-icon" onclick="toggleDropdown()">
                    <div><i class="fa-solid fa-user"></i></div>
                </a>
            </div>  
            <a href="annuaire.php">   
                <div style="margin-bottom: 200%;"><i class="fa-solid fa-address-book"></i></div>
            </a>
            <a href="https://twitter.com/Accueil"> 
            <div class="iconeTwi"><i class="fa-brands fa-twitter"></i></div>
            </a>
            <a href="https://www.instagram.com/">
            <div class="iconeInsta"><i class="fa-brands fa-instagram"></i></div>
            </a>
            <a href="https://fr.linkedin.com/">
            <div class="iconeLin" style="margin-bottom: 250%;"><i class="fa-brands fa-linkedin"></i></div>
            </a>
            <div class="iconeI"><i class="fa-solid fa-circle-info"></i></div>
        </div>
        <?php    
        }
        ?>
        <div class="header">
            <?php 
                echo "<h3>Bonjour, " . $_SESSION['login'] . " !</h3>";
            ?>
        </div>
        <div class="contenu">
            <div class="leftPart">
                <div class="projets" id="carousel1">
                    <!--Diagrammes-->
                    <pie-chart class="item" data="10;10;30" colors="#0d3548;#6d6f70;#385766;" domain="HTML; CSS; JS"></pie-chart>
                    <pie-chart class="item" class="graphique-milieu" data="40;30;30;20;10" domain="PHP; SQL; JS; HTML; CSS"></pie-chart>
                    <pie-chart class="item" data="40;30;20;20;15" colors="#21485c;#879ea9;#0d3548;#385766;" domain="HTML; CSS; JS; C#; Java"></pie-chart>
                    <pie-chart class="item" class="graphique-milieu" data="40;30;30;20;10" domain="PHP; SQL; JS; HTML; CSS"></pie-chart>
                    <pie-chart class="item" data="40;30;20;20;15" colors="#21485c;#879ea9;#0d3548;#385766;" domain="HTML; CSS; JS; C#; Java"></pie-chart>
                    <!-- Flèches de navigation -->
                </div>
                <div class="home">
                    <div class="todo-app">
                         <h2>Liste de choses à faire</h2>
                            <div class="row">
                                <input type="text" id="input-box" placeholder="Ajoute un texte">
                                <button onclick="addTask()">Ajouter</button>
                            </div>
                        <ul id="list-container"></ul>
                    </div>
                    <div class="heure">
                        <div class="cadre">
                            <div class="clock">
                                <div id="hrs">00</div>
                                <div>:</div>
                                <div id="min">00</div>
                            </div>
                        </div>
                    </div>
                    <div class="calendr">
                        <div class="calendar">
                            <div class="left-side">
                                <p id="date">01</p>
                                <p id="day">Sunday</p>
                            </div>
                            <div class="right-side">
                                <p id="month">January</p>
                                <p id="year">2024</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rightPart">
                <div class="notifications"> 
                    <div class="notification-header">
                    <h3 style="color: white; float: left;">Actualités</h3><i style="color: white; float: right; position: relative;" class="fa-solid fa-bell"></i>
                    </div> 
                    <div id="newPostsContainer" class="lesNotifs"></div> <!-- Div pour afficher les actualités -->

<script>
$(document).ready(function() {
    // Fonction pour récupérer et afficher les nouvelles actualités
    function afficherNouvellesActualites() {
        // Effectuer une requête AJAX vers votre fichier PHP qui récupère les actualités
        $.ajax({
            type: 'GET',
            url: 'get_notif.php',
            dataType: 'json',
            success: function(actualites) {
                // Construire le contenu HTML des nouvelles actualités
                var html = '';
                actualites.forEach(function(actualite) {
                    html += '<div class="actualite">';
                    // Vérifier si la publication est récente (moins de 1 jour)
                    var publicationDate = new Date(actualite.creation_date);
                    var oneDayAgo = new Date();
                    oneDayAgo.setDate(oneDayAgo.getDate() - 1);
                    if (publicationDate > oneDayAgo) {
                        html += '<div class="notifs"><span class="new-indicator">NEW</span>' + actualite.description + '</div>';
                    } else {
                        html += '<div class="notifs">' + actualite.description + '</div>';
                    }
                    html += '</div>';
                });
                
                // Afficher les nouvelles actualités dans la zone dédiée
                $('#newPostsContainer').html(html);
            },
            error: function() {
                console.log('Une erreur est survenue lors de la récupération des actualités.');
            }
        });
    }

    // Appeler la fonction pour récupérer et afficher les nouvelles actualités au chargement de la page
    $(document).ready(function() {
        afficherNouvellesActualites();

        // Rafraîchir les actualités toutes les 24 heures (en millisecondes)
        setInterval(afficherNouvellesActualites, 15000); // 24 heures
    });
});
</script>

                </div>
                <div class="app">
                    <div class="box">
                        <a href="https://www.microsoft.com/fr-fr/microsoft-365/excel" style="text-align: center;">
                            <img src="images/excel.png" alt="Description de l'image" style="width: 100%; height: 100%;">
                        </a>
                    </div>
                    <div class="box">
                        <a href="https://gitlab.com/" style="text-align: center;">
                            <img src="images/gitlab.png" alt="GitLab" style="width: 60%; height: 60%;">
                        </a>
                    </div>
                    <div class="box">
                        <a href="https://code.visualstudio.com/" style="text-align: center;">
                            <img src="images/vscode.png" alt="Description de l'image" style="width: 70%; height: 70%;">
                        </a>
                    </div>
                    <div class="box">
                        <a href="https://outlook.office.com/mail/" style="text-align: center;">
                            <img src="images/outlook.png" alt="Description de l'image" style="width: 70%; height: 70%;">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php 
} else {
    header("Location: connexionUser.html");
}?>