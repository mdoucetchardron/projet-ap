const date = document.getElementById("date")
const day = document.getElementById("day")
const month = document.getElementById("month")
const year = document.getElementById("year")

const today = new Date()

const weekDays = ['Dimanche','Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']
const allMonths = ['Janvier','Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août','Septembre', 'Octobre', 'Novembre', 'Décembre']


date.innerHTML = (today.getDate()<10?"0":"") + today.getDate()
day.innerHTML = weekDays[today.getDay()]
month.innerHTML = allMonths[today.getMonth()]
year.innerHTML = today.getFullYear()
