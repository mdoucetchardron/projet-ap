<?php session_start();
if($_SESSION['groupe'] === 'ADM' OR $_SESSION['groupe'] === 'REH'){
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Création d'actualité</title>
    <link rel="stylesheet" href="scriptCreaActualites.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
    <?php include('connexion.php');?>
    <div id="notifications"></div>
    <div class="container">
        <form action="enregistrementActualites.php" method="post" id="notify_form">
            <h2>Création d'actualité</h2>
            <div class="form-group">
                <label for="titre">Titre de l'actualité</label>
                <input type="text" id="titre" name="titre" placeholder="Titre de l'actualité" required>
            </div>
            <div class="form-group">
                <label for="description">Description de l'actualité</label>
                <textarea id="description" name="description" placeholder="Description de l'actualité" rows="5" required></textarea>
            </div>
            <div class="form-group">
                <label for="listeGroupe">Groupe</label>
                <select id="listeGroupe" name="listeGroupe">
                    <?php
                    // On sélectionne tous les groupes de la base de données
                    $requete = $connexion->query("SELECT * FROM groupe");

                    // On parcours les résultats de la table groupe, et on affiche les options de la liste déroulante
                    while($groupe = $requete->fetch()) {
                        echo "<option value='" . $groupe['codeGrp'] . "'>" . $groupe['libelle'] . "</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <button type="submit">Créer Actualité</button>
                <button type="reset">Annuler</button>
                <button onclick="window.location.href='siteintranet.php'" style="float: right;">Retour</button>
            </div>
        </form>
    </div>
</body>
</html>
<?php
} else {
    header("Location: connexionUser.html");
}
?>

<!--
                            $req = $connexion->prepare('SELECT * FROM actualites WHERE grp = :legroupe ORDER BY id DESC');
                            $req->bindParam(':legroupe', $_SESSION['groupe']);
                            $req->execute();
                            
                            while ($actualite = $req->fetch()) {
                                // Vérifier si l'actualité est nouvelle (publiée il y a moins de 3 jours)
                                $publicationDate = strtotime($actualite['creation_date']);
                                $threeDaysAgo = strtotime('-3 days');
                                
                                if ($publicationDate > $threeDaysAgo) {
                                    // Afficher l'indicateur "NEW"
                                    echo "<div class='notifs'><span class='new-indicator'>NEW</span>" . $actualite['description'] . "</div>";
                                } else {
                                    // Actualité plus ancienne, pas besoin de l'indicateur "NEW"
                                    echo "<div class='notifs'>" . $actualite['description'] . "</div>";
                                }
                            }                        
                        ?>
-->
                        
