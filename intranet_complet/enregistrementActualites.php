<?php 
include("connexion.php");


if (!empty($_POST["titre"]) && !empty($_POST["description"]) && !empty($_POST["listeGroupe"])){
    $titre = $_POST["titre"];
    $description = $_POST["description"];
    $groupe = $_POST["listeGroupe"];

    $insertion = $connexion->prepare('INSERT INTO actualites (titre,description, grp) VALUES (:titre,:description, :grp)');
    $insertion->bindParam(':titre', $titre);
    $insertion->bindParam(':description', $description);
    $insertion->bindParam(':grp', $groupe);
    $insertion->execute();

    header("Location: creActualites.php");
}
?>