<?php
// Démarage de la session 
session_start();

// Insertion du formulaire de connexion à la base de données
include('connexion.php'); 

// Vérifiaction des champs de connexion 
if(!empty($_POST['username']) && !empty($_POST['mdp'])){
    // Récupération des valeurs saisies dans le formulaire
    $login = $_POST['username'];
    $mdp = $_POST['mdp'];

    //Vérification des informations 
    $requete = $connexion->prepare("SELECT * FROM utilisateurs WHERE login = :login");
    $requete->bindParam(':login',$login);
    $requete->execute();
    $resultat = $requete->fetch();
    $_SESSION['groupe'] = $resultat['codeGrp'];

    if($resultat && password_verify($mdp, $resultat['password'])) {
        // Enregistrement du login dans la variable de session 
        $_SESSION['login'] = $login;

        // Redirection de l'utilisateur
        header("Location: siteintranet.php");
        exit;
    } else {
        // Identifiant ou mot de passe incorrect
        echo "Identifiant ou mot de passe incorrect. Veuillez réessayer."; 
    } 
}else {
        // Les champs sont vides
        echo "Veuillez saisir les champs demandés.";
}
?>   