<?php
//connexion
$dbhost = "localhost";
$dbport = 3307;
$db = "corpany_project_intranet";
$dbuser = "dupont";
$dbpasswd = "secret";

try {
    $connexion = new PDO("mysql:host=$dbhost;port=$dbport;dbname=$db", $dbuser, $dbpasswd);
    $connexion->exec("SET CHARACTER SET utf8");
} catch(PDOException $e) {
    echo 'Erreur de connexion : ' . $e->getMessage();
}
?>
