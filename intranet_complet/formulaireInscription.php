<?php
//Insertion du fichier connexion.php
include('connexion.php');

//Récupération des données saisies par l'utilisateur 
if (!empty($_POST['login']) && !empty($_POST['password']) && !empty($_POST['listeGroupe'])) {
    $login = $_POST['login'];
    $password = $_POST['password'];
    $codeGrp = $_POST['listeGroupe']; // Récupération de la valeur sélectionnée dans la liste déroulante

    // Hachage du mot de passe
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

    //Insertion des données dans la base de données
    try {
        $insertion = $connexion->prepare('INSERT INTO utilisateurs (login, password,codeGrp) VALUES (:login, :password, :codeGrp)');
        $insertion->bindParam(':login', $login);
        $insertion->bindParam(':password', $hashedPassword);
        $insertion->bindParam(':codeGrp', $codeGrp); // Insertion du codeGrp
        $insertion->execute();

    } catch (PDOException $e) {
        echo 'Erreur de requête : ' . $e->getMessage();
    }
    header("Location: connexionUser.html");
    exit;
} 
?>

