<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
    <link rel="stylesheet" href="styleInscription.css">
    <script src="validationInscription.js" defer></script>
</head>
<body>
<?php include('connexion.php');?>
    <form action="formulaireInscription.php" method="post" name="formInscription">
    <div class="container">
            <div class="left-side">
                <h1 class="texte-en-gras">Bienvenue</h1>
                <p class="policeMonospace">Sur le site de l'entreprise</p>
            </div>
            <div class="right-side">
                <p>
                    <div class="input-group">
                    <h2 id="username-wrapper" class="texte-en-gras">S'inscrire</h2>
                        <input type="text" id="username" name="login" placeholder="Nom d'utilisateur" required/>
                    </div>
                    <div id="password-wrapper" class="input-group">
                        <input type="password" id="password" name="password" placeholder="Mot de passe" required/>
                    </div>    
                </p>
                    <label for="listeGroupe">Choisissez un groupe :</label>
                    <select name ="listeGroupe" id="listeGroupe">
                        <?php
                        // On sélectionne tous les groupes de la base de données
                        $requete = $connexion->query("SELECT * FROM groupe");

                        // On parcours les résultats de la table groupe, et on affiche les options de la liste déroulante
                        while($groupe = $requete->fetch()) {
                            echo "<option value='" . $groupe['codeGrp'] . "'>" . $groupe['libelle'] . "</option>";
                        }
                        ?>
                    </select>
                <p>
                    <button type="submit" id="verifier">S'inscrire</button>
                    <button type="reset">Annuler</button>
                </p>
            </div>
        </div>
    </form>
</body>
</html>
