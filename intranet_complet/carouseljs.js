class Carousel {

    /**
     * @param {HTMLElement} element 
     * @param {Object} options
     * @param {Object} options.slidesToScroll Nombre d'éléments à faire défiler
     * @param {Object} options.slidesVisible Nombre d'éléments visibles dans un slide
     * @param {boolean} options.loop Doit-t-on boucler en fin de carousel
     */
    constructor (element, options = {}) {
        // Initialisation des éléments et des options du carousel 
        this.element = element
        this.options = Object.assign({}, {
            slidesToScroll: 1,
            slidesVisible: 1,
            loop: false
        }, options)
        let children = [].slice.call(element.children)
        this.currentItem = 0
        this.root = this.createDivWithClass('carousel')
        this.container = this.createDivWithClass('carousel__container')
        this.root.appendChild(this.container)
        this.element.appendChild(this.root)
        this.moveCallbacks = []
        this.items = children.map((child) => {
            let item = this.createDivWithClass('carousel__item')
            item.appendChild(child)
            this.container.appendChild(item)
            return item
        })
       // Appel de la méthode pour initialiser le style du carousel
       this.setStyle();

       // Appel de la méthode pour créer les flèches de navigation
       this.createNavigation();
   
       // Appel de la méthode pour allumer le premier indicateur
       this.highlightIndicator(0);    

    }


    // Méthode pour mettre en surbrillance un indicateur spécifique
    highlightIndicator(index) {
        const indicators = this.root.querySelectorAll('.carousel__indicator');
        indicators.forEach((indicator, i) => {
            if (i === index) {
                indicator.classList.add('highlighted');
            } else {
                indicator.classList.remove('highlighted');
            }
        });
    }

    /**
     * Applique les bonnes dimensions aux éléments du carousel 
    */ 
    setStyle() {
        let ratio = this.items.length / this.options.slidesVisible;
        this.container.style.width = (ratio * 100) + "%";
        this.items.forEach(item => item.style.width = ((100 / this.options.slidesVisible) / ratio) + "%"); 
    }

    createNavigation (){
        let indicatorsContainer = this.createDivWithClass('carousel__indicators')
        let nextButton = this.createDivWithClass('carousel__next')
        let prevButton = this.createDivWithClass('carousel__prev')
        //Ajout des icônes des flèches
        nextButton.innerHTML = '<i class="fa-solid fa-angle-right"></i>'
        prevButton.innerHTML = '<i class="fa-solid fa-angle-left"></i>'
        this.root.appendChild(nextButton)
        this.root.appendChild(prevButton)
        this.root.appendChild(indicatorsContainer)

        // Création des indicateurs
        for (let i = 0; i < Math.ceil(this.items.length / this.options.slidesToScroll); i++) {
            let indicator = this.createDivWithClass('carousel__indicator');
            indicatorsContainer.appendChild(indicator);
        }

        // Ajout des écouteurs d'évènements pour les clics sur les flèches
        nextButton.addEventListener('click', this.next.bind(this))
        prevButton.addEventListener('click', this.prev.bind(this))

        // Cacher la flèche 'prev' si currentItem est égal à zéro
        if (this.currentItem === 0) {
            prevButton.classList.add('carousel__prev--hidden');
        }

        this.onMove(index => {
            if (index === 0) {
                prevButton.classList.add('carousel__prev--hidden')
            } else {
                prevButton.classList.remove('carousel__prev--hidden')
            }
        
            if (this.items[this.currentItem + this.options.slidesVisible] === undefined) {
                nextButton.classList.add('carousel__next--hidden')
            } else {
                nextButton.classList.remove('carousel__next--hidden')
            }
        })
        
    }

    next () {
        this.gotoItem(this.currentItem + this.options.slidesToScroll)
    } 

    prev () {
        // Vérification de si l'élément actuel est déjà au début (index 0)
        if (this.currentItem === 0 && !this.options.loop) {
            return;
        }

        this.gotoItem(this.currentItem - this.options.slidesToScroll)
    }

    /**
     * Déplace le carousel vers l'élément cible
     * @param {number} index
     */
    
    gotoItem (index) { 
        const totalPages = Math.ceil(this.items.length / this.options.slidesToScroll)
        const currentPage = Math.floor(index / this.options.slidesToScroll)
        // Mettre à jour les indicateurs pour mettre en surbrillance la page actuelle
        this.highlightIndicator(currentPage)       
        if (index < 0) {
            index = this.items.length - this.options.slidesVisible
        }else if (index >= this.items.length || this.items[this.currentItem + this.options.slidesVisible] === undefined) {
            index = 0
        } 
        let translateX = index * -100 / this.items.length
        this.container.style.transform = 'translate3d(' + translateX + '%, 0, 0)'
        this.currentItem = index
        this.moveCallbacks.forEach(cb => cb(index))
        
    }

    
    onMove (cb) {
        this.moveCallbacks.push(cb)
    }

    /**
     * 
     * @param {string} className 
     * @returns {HTMLElement}
     */
    createDivWithClass(className) {
        let div = document.createElement('div')
        div.setAttribute('class', className)
        return div
    }
}

document.addEventListener('DOMContentLoaded', function () {

    let carousel = new Carousel(document.querySelector('#carousel1'), {
        slidesVisible: 3,
        slidesToScroll:3,
        loop: false
    })

    carousel.setStyle(); // Appel de la méthode setStyle pour ajuster la largeur de la container
})