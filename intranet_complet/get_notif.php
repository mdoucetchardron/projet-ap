<?php
session_start();
include('connexion.php');

$idGroupeUtilisateur = $_SESSION['groupe'];

// Requête pour sélectionner les actualités créées il y a moins de 1 jour
$query = $connexion->prepare("SELECT * FROM actualites WHERE grp = :idGroupe ORDER BY id DESC");
$query->bindParam(':idGroupe', $idGroupeUtilisateur);
$query->execute();
$actualites = $query->fetchAll();

// Envoyer la réponse JSON avec les actualités récupérées
header('Content-Type: application/json');
echo json_encode($actualites);
?>