function toggleDropdown() {
    var dropdownContent = document.getElementById("dropdownContent");
    dropdownContent.classList.toggle("active");
}

// Gérer dynamiquement le contenu du menu déroulant
document.getElementById("dropdownMenu").addEventListener("click", () => {
    var dropdownMenu = document.getElementById("dropdownMenu");
    var dropdownContent = document.createElement("div");
    dropdownContent.className = "dropdown-content";
    dropdownContent.id = "dropdownContent";
    dropdownContent.innerHTML = `
        <a href="voirProfil.php">Voir le profil</a>
        <a href="deconnexion.php">Déconnexion</a>
    `;
    dropdownMenu.appendChild(dropdownContent);
})

// Fermer le menu déroulant si l'utilisateur clique en dehors de celui-ci
window.onclick = function(event) {
    if (!event.target.closest('.dropdown')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('active')) {
                openDropdown.classList.remove('active');
            }
        }
    }
}