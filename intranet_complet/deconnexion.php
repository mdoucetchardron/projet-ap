<?php
// Démarrer la session
session_start();

// Supprimer les variables de session 
unset($_SESSION);

// Terminer la session
session_destroy();

// Redirection vers la page de connexion 
header("Location: connexionUser.html");
exit;
?>