-- Création de la base de données
CREATE DATABASE IF NOT EXISTS corpany_project_intranet DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE corpany_project_intranet;

-- Création de l'utilisateur avec les droits sur la base de données
CREATE USER 'dupont'@'localhost' IDENTIFIED BY 'secret';

-- Attribution de tous les droits sur la base de données à l'utilisateur
GRANT ALL PRIVILEGES ON corpany_project_intranet.* TO 'dupont'@'localhost';

-- Création des tables 
CREATE TABLE groupe (
  codeGrp char(3) NOT NULL,
  libelle varchar(30) DEFAULT NULL,
  PRIMARY KEY (codeGrp)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE poste (
  idposte char(3) NOT NULL,
  libelleposte varchar(255) DEFAULT NULL,
  PRIMARY KEY (idposte)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE utilisateurs (
  login varchar(50) NOT NULL,
  password varchar(255) NOT NULL,
  codeGrp char(3) NOT NULL,
  PRIMARY KEY (login),
  KEY fk_groupe_utilisateurs (codeGrp)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE infosemployes (
  nom varchar(50) NOT NULL,
  prenom varchar(50) NOT NULL,
  login varchar(50) NOT NULL,
  leposte char(3) NOT NULL,
  chemin_image varchar(255) DEFAULT NULL,
  tel int(10) DEFAULT NULL,
  mail varchar(255) DEFAULT NULL,
  PRIMARY KEY (nom),
  KEY fk_infosemployes_utilisateurs (login),
  KEY fk_idposte_leposte (leposte)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE actualites (
  id int(11) NOT NULL AUTO_INCREMENT,
  titre varchar(255) NOT NULL,
  description text NOT NULL,
  grp char(3) DEFAULT NULL,
  creation_date timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (id),
  KEY fk_groupe_actualites (grp)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Ajout des contraintes 
ALTER TABLE infosemployes
  ADD CONSTRAINT fk_idposte_leposte FOREIGN KEY (leposte) REFERENCES poste (idposte),
  ADD CONSTRAINT fk_infosemployes_utilisateurs FOREIGN KEY (login) REFERENCES utilisateurs (login);

ALTER TABLE utilisateurs
  ADD CONSTRAINT fk_groupe_utilisateurs FOREIGN KEY (codeGrp) REFERENCES groupe (codeGrp);

ALTER TABLE actualites
  ADD CONSTRAINT fk_groupe_actualites FOREIGN KEY (grp) REFERENCES groupe (codeGrp);

-- Insertion des premières données 
INSERT INTO groupe (codeGrp, libelle)
VALUES ('ADM', 'Administrateur'), 
('INF', 'Informatique'), 
('LOG', 'Logistique'), 
('REH', 'Ressources Humaines');

INSERT INTO poste (idposte, libelleposte)
VALUES ('DIR', 'Directeur'),
('EMP', 'Employé'),
('RES', 'Responsable équipe');
