# Le Site de : AUGEREAU-BAHUAUD-BINET-DOUCET
Ce site est ce que l'on appelle un Intranet, un intranet est un réseau informatique privé utilisé par les employés d'une entreprise ou de toute autre entité organisationnelle et qui utilise les mêmes protocoles qu'Internet.
Nous réalisons ce site dans le cadre d'un projet de notre 1ère année de BTS SIO.

## Nos fichiers récents :
- [ ] [Notre fichier UML](UML/UML.png)


## Qu'est-ce que GitLab ?
GitLab est une plateforme DevOps open source qui s’appuie sur le logiciel de gestion de versions Git. Elle permet de créer, tester et déployer des logiciels de manière collaborative. L’application tout-en-un propose d’accéder à l’ensemble des étapes du cycle de vie DevOps : chaque équipe peut planifier des projets et gérer le code source, l’approche CI/CD ainsi que la sécurité. 
GitLab permet de : gérer du code source, panifier, sécuriser et permettre une intégration et une distribution continue.

## Installation :
Pour que vous puissiez utiliser notre Intranet, il est nécessaire que :
<<<<<<< HEAD
- le dossier "intranet_A_B_B_D" se trouve dans le dossier **"www"**
- **http** et **MariaDB** devront être démarrés
- de disposer du dossier "FichierSQL_intranet"

Dans le dossier FichierSQL_intranet se trouve : 
- le fichier "BDD_intranet"
- le fichier "insertion2.sql"

A partir de PhpMyAdmin : 
- se connecter à son compte, puis importer le fichier "BDD_intranet". Celui-ci créera la base de données, les tables, puis un utilisateur 'dupont' qui sera utilisé pour gérer la base de données de l'intranet. 

=======
- Que le dossier Intranet se trouve dans le dossier **"www"**
- Que **http** soit démarré
- Que **MariaDB** soit démarré
>>>>>>> 542eed1db34128a5a08ea8882013924095ff10aa

## Lancer notre Intranet :
Pour le lancer, il suffit de 


## Utilisation de notre Intranet :
1. Après avoir correctement effectué au préalable la partie **"Installation"** :
<<<<<<< HEAD
2. Vous devez commencer par vous **créer un compte**.
3. Pour cela, dans la barre de votre navigateur, vous entrez le lien suivant : [http//localost](http://localhost/intranet_A_B_B_D/inscriptionUser.php)
4. Vous arriverez sur une page qui permettra de créer votre compte, en insérant le nom d'utilisateur, le mot de passe, en n'oubliant pas de choisir votre groupe. Il faut obligatoirement remplir les champs, sinon vous ne pouvez pas vous inscrire. 
5. Une fois le compte créé, vous serez redirigez automatiquement sur la page de connexion. Il vous suffit d'entrez votre identifiant ainsi que votre mot de passe.
3. Connecté, vous accédez à votre **page personnalisée** :
     - Un message de politesse, avec votre login 
     - un menu avec plusieurs icônes qui permettent d'accèder à différentes pages
     - une représentation en diagrammes de l'avancé de ses projets personnels 
     - Une liste personnelle de "choses à faire"
     - Un ensemble d'actualités pouvant faire référence à de récentes mise à jours de logiciels, ou à des nouveautés dans l'entreprise ...
     - Les outils utilisés par l'Entreprise (excel, vsc...)
     - L'heure et la date actuelle

Détail de la page d'accueil : 

Le menu contient : 
- une icône "profil" qui en double cliquant permet d'accèder à son profil, ou se déconnecter. 
(La page profil n'existe pas, mais elle devra permettre à l'utilisateur d'avoir accès aux données personnels que l'entreprise possède sur lui)
- une icône "annuaire" qui en cliquant permet d'accèder à l'annuaire de l'entreprise, avec tous ses employés
- trois icônes "réseaux sociaux". Au clic, vous serez redirigez sur leurs sites réspectifs.
(Pour une entreprise existante, vous seriez redirigé sur la page d'accueil du compte de l'entreprise)
- une icône "i" qui devra renseigner l'utilisateur sur l'utilisation du site

- une icône "stylo plume" si l'utilisateur appartient au groupe "Administrateur" ou "Ressources Humaines". Cette icône renverra vers une page qui sera également accessible que par les groupes précédemment sités, afin de créer des actualités. 

Le reste de la page : 
- le widget contenant l'avancé des projets n'est qu'une représentation dynamique de diagrammes. il est possible de les faire défiler avec la flèche de droite, puis de gauche. A noter que ces flèches disparaissent s'il n'y a plus de diagrammes à faire défiler. 
(Dans la réalité, l'avancé des projets, pour chaque utilisateur, sera enregistré dans la base de données de l'entreprise. Une fonctionnalité pour modifier l'avancé sera accessible par l'utilisateur. On pourra utiliser du AJAX et du php)

- le widget contenant une liste de choses à faire est disponible pour chaque utilisateur. Elle a pour objectif de permettre à l'utilisateur de gérer sa journée de travail comme il le souhaite. Possibilité d'ajouter une tâche en cliquant sur le bouton "Ajouter", de la cocher en cliquant sur cette dernière, puiis de la supprimer avec la croix.
(Dans ce cas précis, tous les utilisateurs possèdent la même liste de chose à faire. Pour qu'elle soit propre à l'utilisateur, les informations seraient insérés dans la base de données)

- le widget contenant les Actualités est disponible en fonction du groupe dans lequel appartient l'utilisateur. Ici, un maximum de quatre actualités est disponibles visuellement (elles sont supprimées pour l'utilisateur mais restent disponibles dans la base de données). Une petite icône est présente sur l'actualité lorsque celle-ci a été crée il y a moins d'un jour, elle disparaîtra alors après cette durée dépassée. 
(Ici, nous avons utilisé du AJAX pour que l'ajout des actualités ainsi que l'icône "NEW" s'actualisent tous seuls, sans que l'utilisateur n'ai à rafraîchir la page. A long terme il faudrait envisager de créer un système de notifications, avec une notif qui s'affiche lorsqu'une nouvelle actualité est créée. C'est la tâche qui a été la plus difficile a réalisée, créer un système de notifications.)

- le widget contenant les différents logiciels redirigent au clic, l'utilisateur vers leurs sites réspectifs. 
(L'entreprise utilisera sans doutes un nombre dépassant quatre logiciels, c'est pourquoi nous mettrons un système de carousel, comme pour les diagrammes, afin de les faire défiler.) 

Les pages respectives de chaque icône : 

La page contenant l'annuaire :
- Possède un menu équivalent à la page principale de l'intranet, avec en plus une icône "maison" à la place de l'icône "annuaire" qui permet à l'utilisateur de retourner à la page principale. Cet annuaire contient les différents employés, en fonction de leur groupe, avec des pastilles de couleurs qui indiquent si la personne est le directeur, un responsable d'équipe, ou un employé. 
Pour pouvoir accèder à tout cela il faudra préalablement créer les comptes via l'interface utilisateur (fichier "connexionUser.html") avec les logins présents dans le fichier "insertion2.sql". Ce fichier contient toutes les informations des employés, qui seront alors affichés dans l'annuaire. (Possibilité d'insérer les données avec le compte "dupont")
(Ceci est un mode d'utilisation temporaire. Afin d'afficher les informations des utilisateurs, une interface permettra à un employé de l'administration, d'enregistrer toutes les informations)

la page de création d'actualités : 
- cette page permet de créer les actualités qui seront alors affichées dans le widget "Actualités". Les champs doivent tous être remplits sans exceptions, sans oublier de choisir le groupe. Une fois les champs complétés, on clique sur le bouton "Créer Actualité". Le bouton "Annuler" permet de réinitialiser les champs, et le bouton "retour" permet de revenir sur la page princiaple de l'intranet. 

On aimerait : 
- rajouter d'autres fonctionnalités comme un agenda pour chaque utilisateur, qu'il pourra compléter en ajoutant ces tâches, ces réunions, rendez-vous ... 
- faire évoluer le design du site (mode sombre ou mode clair)

=======
2. Vous devez commencer par vous **connecter**, ou **créer un compte** si vous n'en avez pas.
3. Une fois connecté, vous accédez à votre **page personnalisée** :
     - Des notifications et des actualités personnalisées
     - Une liste personnelle de "choses à faire"
     - Les outils utilisés par l'Entreprise (excel, vsc...)
     - Un accès aux reéseaux sociaux de l'Entreprise
     - L'heure et la date actuelle
     - L'annuaire de l'Entreprise
4. Possibilité de **déconnexion**
>>>>>>> 542eed1db34128a5a08ea8882013924095ff10aa

## Représentation UML de l'utilisation de l'Intranet :
[ **Cliquez ici pour voir notre fichier UML** ](UML/UML.png)


## Avancement de notre projet au fil du temps : 
**1. Etude de marché et recherche de fonctionnalités.**

**2. Mise en place du projet sur GitLab.**

**3. Création d'une maquette pour l'Intranet** (accès maquette : https://gitlab.com/Eliottusa/le_site_incroyable_de_augereau_bahuaud_binet_doucet/-/tree/main/Maquette?ref_type=heads ).

**4. Réalisation de vues HTML en tentant de bien représenter les maquettes.**

**5. Installation de l'environnement :** environnement de développement pour PHP, création de base des données, fichier PHP test d'accès à la base de données.

**6. Analyse d'une première fonctionnalité :** Créer des représentation UML des relations et des fonctionnalités et création de tables dans la BDD.

**7. Conception d'une première fonctionnalité :** Création du formulaire HTML pour la création d'un utilisateur, ajout des groupes dans la table Groupe de la BDD. Ajout de code PHP pour récupérer les groupes de la BDD et les valeurs du formulaire pour créer l'utilisateur dans le BDD.

**8. Authentification des utilisateurs :** Mise à jour de l'UML, réalisation du formulaire de connexion avec PHP et redirection vers tableau de bord. Création de variables de sessions pour stocker les informations. Mise en place d'une protection pour le tableau de bord, qu'on ne puisse pas y accéder si l'on n'a pas de compte. Ajout d'un boutun de deconnexion

**9. Intégration des actualités :** Création d'une table actualité dans la BDD et d'un lien par PHP avec le tableau de bord, afin que celles-ci s'affichent sur celui-çi.

**10. Intégration de la création d'actualités et de notifications :** Mise à jour de l'UML, compte tenu des nouvelles fonctionnalités disponibles. Adaptation de la BDD et intégration du code PHP.

**11. Intégration de l'organigramme fonctionnel :** Mise à jour finale de l'UML, réadaptation de la BDD et intégration du code PHP.

## Logiciels utilisés pour le projet :
-  **Virtual Studio Code**
-  **phpMyAdmin** 
     

## Comment participer à notre projet ?
Vous pouvez participer à notre projet en nous aidant à améliorer notre code source si l'envie vous prend !!

## FAQ : Questions les plus fréquentes :
- [ ]
- [ ]
<<<<<<< HEAD
=======

## Contact avec le groupe :

- [ ] Eliott AUGEREAU : eaugereau@la-joliverie.com
- [ ] Maïwenn DOUCET : mdoucetchardron@ja-joliverie.com
- [ ] Alexis BAHUAUD : abahuaud@la-joliverie.com
- [ ] Morgane BINET : mbinet@la-joliverie.com
>>>>>>> 542eed1db34128a5a08ea8882013924095ff10aa

## Contact avec le groupe :

- [ ] Eliott AUGEREAU : eaugereau@la-joliverie.com
- [ ] Maïwenn DOUCET : mdoucetchardron@ja-joliverie.com
- [ ] Alexis BAHUAUD : abahuaud@la-joliverie.com
- [ ] Morgane BINET : mbinet@la-joliverie.com
